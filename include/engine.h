#pragma once
#include <application.h>

namespace modo {
    class Engine {
        public:
            // Singleton
            static Engine& getInstance()
            {
                static Engine instance;
                return instance;
            }

            Application* m_app;

            static int initialize(Application* app);

        private:
            Engine() {}

        public:
            Engine(Engine const&)               = delete;
            void operator=(Engine const&)       = delete;

            // Note: Scott Meyers mentions in his Effective Modern
            //       C++ book, that deleted functions should generally
            //       be public as it results in better error messages
            //       due to the compilers behavior to check accessibility
            //       before deleted status
    };
}