#include <iostream>
#include <modo.h>

namespace modo {
    int initialize(Application* app) {
        return Engine::initialize(app);
    }
}