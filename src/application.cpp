#include <application.h>
#include <iostream>

namespace modo {
    Application::Application() {}
    Application::~Application() {}

    void Application::say_hello() {
        std::cout << "Hello from the Application class" << std::endl;
    }
}