#include <engine.h>

namespace modo {
    int Engine::initialize(Application* app) {
        getInstance().m_app = app;
        getInstance().m_app->say_hello();

        return 1;
    }
}